import pandas as pd
from sklearn import tree
from sklearn.externals import joblib
from sklearn.preprocessing import Imputer
from sqlalchemy import create_engine

if __name__ == '__main__':
    db_conection = create_engine('sqlite:///../backend/sqlite3.db')

    # Grab Startups
    startups = pd.read_sql_table(
        'startups',
        db_conection,
        index_col='id',
        columns=['number_of_employees', 'total_funding_raised']
    )

    # Grab Industries
    industries = pd.read_sql_table('startup_industries', db_conection, index_col='id')
    industries = industries.drop_duplicates('startup_id')

    # Grab locations
    locations = pd.read_sql_table('startup_locations', db_conection, index_col='id')
    locations = locations.drop_duplicates('startup_id')

    # Add Industries/Locations to Startups
    startups = pd.merge(
        startups,
        industries,
        how='left',
        left_index=True,
        right_on='startup_id'
    )
    startups = pd.merge(startups, locations, how='left', on='startup_id')

    # Preprocess data
    imp = Imputer(missing_values='NaN', strategy='mean', axis=0)
    X = imp.fit_transform(
        startups[['number_of_employees', 'total_funding_raised',  'industry_id', 'location_id']].as_matrix()
    )
    Y = startups[['startup_id']].as_matrix()

    clf = tree.DecisionTreeClassifier()
    clf = clf.fit(X, Y)

    joblib.dump(clf, 'startups.pkl')
