# Classifier

Get classified!

## Setup

1. Make a python virtual env
```bash
mkvirtualenv --python=$(which python) classifier
```

2. Install python requirements
```bash
pip install -r requirements.txt
```

3. Run server
```bash
$ export FLASK_APP=app.py
$ flask run
 * Running on http://127.0.0.1:5000/
```
